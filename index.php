<?php

$searchWords = array('php','html','интернет','Web');

$searchStrings = array(
    'Интернет - большая сеть компьютеров, которые могут взаимодействовать друг с другом.',
    'PHP - это распространенный язык программирования с открытым исходным кодом.',
    'PHP сконструирован специально для ведения Web-разработок и его код может внедряться непосредственно в HTML'
);


function searchInStrings($words, $strings)
{
    foreach ($strings as $key => $string) {

        $inString = '';

        foreach ($words as $word) {

            if (preg_match('/' . $word . '/iu', $string)) {
                $inString .= $word . '. ';
            }

        }

        echo 'В предложении №' . ($key + 1) . ' ' . 'есть слова: ' . $inString . '<br>';
    }
}


searchInStrings($searchWords, $searchStrings);